var expect = require("chai").expect;
var should = require('chai').should();
var reader = require("../../src/lib/reader");

describe("Reader", function(){
    describe(".readFile()", function() {
        it("should return error on invalid filename", function(){
            var actual = reader.readFile();

            expect(actual).to.be.an.instanceof(Error);
        });
    });
});

describe("Reader", function(){
    describe(".readFile()", function() {
        it("should return error on invalid callback", function(){
            var actual = reader.readFile('test/data/valid/input', new Object());

            expect(actual).to.be.an.instanceof(Error);
        });
    });
});

describe("Reader", function(){
    describe(".readFile()", function() {
        it("should pass file content to callback", function(){
            var callback = function(error, content) {
                should.not.exist(error);
                content.should.be.an('Object');
            };

            reader.readFile('test/data/valid/input', callback);
        });
    });
});
