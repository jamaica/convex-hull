var expect = require("chai").expect;
var assert = require("chai").assert;
var points = require("../../src/lib/points");

describe("Points", function(){
    describe(".parsePoints()", function() {
        it("should return points on valid input", function(){
            var expected = [ [2,7], [4,7], [3,9] ];

            var actual = points.parsePoints('2 7\n4 7\n3 9');
            expect(actual).to.eql(expected);
        });
    });
});

describe("Points", function(){
    describe(".parsePoints()", function() {
        it("should return filter out invalid points", function(){
            try {
                var actual = points.parsePoints('2\n4 7\n3 9');
            } catch (e) {
                expect(e).to.eql( new Error());
            }
        });
    });
});

describe("Points", function(){
    describe(".parsePoints()", function() {
        it("should return empty array on invalid input", function(){
            try {
                points.parsePoints(new Object());
            } catch (e) {
                expect(e).to.eql( new Error());
            }


        });
    });
});