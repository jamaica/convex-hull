var expect = require("chai").expect;
var graham = require("../../src/lib/graham-alg");

describe("Graham", function(){
    describe(".getHull()", function() {
        it("Should return correct Convex Hull on valid data", function(){
            var input = getDataFile('test/data/valid/input');
            var expected = getDataFile('test/data/valid/expected');

            var actual = graham.getHull(input);

            expect(expected).to.deep.include.members(actual);
        });
    });
});

describe("Graham", function(){
    describe(".getHull()", function() {
        it("Should have n log n complexity", function(){
            var input = getDataFile('test/data/big/input');
            var expected = getDataFile('test/data/big/expected');

            var actual = graham.getHull(input);

            expect(expected).to.deep.include.members(actual);
        });
    });
});

describe("Graham", function(){
    describe(".getHull()", function() {
        it("Should return correct Convex Hull for edge cases", function(){

            var dataSets = [
                {
                    expected: [[1, 1], [5, 6], [8, 9]],
                    data: [[1, 1], [5, 6], [8, 9]]
                }, {
                    expected: [],
                    data: []
                }, {
                    expected: [[1,1], [1,3]],
                    data: [[1,1], [1,2], [1,3]]
                }, {
                    expected: [[1,1], [3,1]],
                    data: [[1,1], [2,1], [3,1]]
                },
            ];

            dataSets.forEach(function( dataSet) {
                var actual = graham.getHull(dataSet['data']);

                expect(dataSet['expected']).to.eql(actual);

            });
        });
    });
});

describe("Graham", function(){
    describe(".isLeftTurn()", function() {
        it("should return positive int for a left point", function(){
            var result = graham.isLeftTurn([1, 1], [2, 2], [1,2]);

            expect(result).to.be.above(0);
        });
    });
});

describe("Graham", function(){
    describe(".isLeftTurn()", function() {
        it("should return zero for aligned", function(){
            var result = graham.isLeftTurn([1, 1], [2, 2], [3,3]);

            expect(result).to.equal(0);
        });
    });
});

describe("Graham", function(){
    describe(".isLeftTurn()", function() {
        it("should return negative int for a right point", function(){
            var result = graham.isLeftTurn([1, 1], [2, 2], [2,1]);

            expect(result).to.equal(-1);
        });
    });
});

describe("Graham", function(){
    describe(".sort()", function() {
        it("should return sorted by angle points", function(){
            /** isLeftTurn should be mocked.*/
            var dataSets = [
                {
                    anchor : [1, 1],
                    expected: [[ 8.9 ], [ 0.23, 0, 15 ], [ 7, 5 ], [ 5, 6 ], [ 5, 12 ]],
                    data: [[8.9], [5, 6], [0.23, 0,15], [7, 5], [5, 12] ]
                },
            ];

            dataSets.forEach(function( dataSet) {
                graham.sort(dataSet['data'], dataSet['anchor']);

                expect(dataSet['data']).to.eql(dataSet['expected']);

            });
        });
    });
});

function getDataFile(path) {
    var fs = require('fs');
    var points = require("../../src/lib/points");
    return points.parsePoints(fs.readFileSync(path).toString());

}