function processInput( err, files) {
    var graham = require('./lib/graham-alg.js');
    var points = require('./lib/points.js');
    if(err) {
        return console.log("File load failed." + err);
    }

    try {
        var input = points.parsePoints(files.toString());
        console.log("Input points:\n", input);
        var hull = graham.getHull(input);
        console.log('Convex Hull:\n', hull);
    } catch (e) {
        return console.log(e);
    }
}

reader = require('./lib/reader.js');
var s = reader.readFile(process.argv[2], processInput);
(s instanceof Error) && console.log(s);