/**
 * @desc Calculates a Convex Hull for given set of points using Graham Algorithm.
 * @url http://en.wikipedia.org/wiki/Graham_scan
 *
 * @author Arthem Nazarenko
 * @version 1.0.0
 */

/**
 * Calculates Convex hull of a set of points.
 * If number of points is less than 4 - joining them would be a Convex Hull
 *
 * @param points
 * @returns {*[]}
 */
module.exports.getHull = function(points) {
    if (points.length == 3 ) {
        var elemX = points.map(function (a) {
            return a[0];
        }).filter(unique);
        var elemY = points.map(function (a) {
            return a[1];
        }).filter(unique);
        if (elemX.length == 1) {
            var minY = Math.min.apply(null, elemY);
            var maxY = Math.max.apply(null, elemY);
            return [[elemX[0], minY], [elemX[0], maxY]]
        }
        if (elemY.length == 1) {
            var minX = Math.min.apply(null, elemX);
            var maxX = Math.max.apply(null, elemX);
            return [[minX, elemY[0]], [maxX, elemY[0]]]
        }
    }
    if (points.length < 4) {
        return points;
    }
    var anchor = points[0];
    for (var i = 1; i < points.length; i++) {
        if (points[i][1] < anchor[1]) {
            anchor = points[i];
        }
    }

    points.splice(points.indexOf(anchor), 1);
    sort(points, anchor);
    points.unshift(anchor);
    return calculateHull(points);
}

module.exports.sort = sort = function (points, anchor) {
    points.sort(function (a,b) {
        var left = isLeftTurn(anchor, a, b);
        if (left < 0) {
            return 1;
        } else {
            return -1;
        }
    });
}

/**
 * Removes points that are inside of Convex Hull
 *
 * @param [[]] items
 * @returns {*[]}
 */
module.exports.calculateHull = calculateHull = function (items) {
    var hp = 1;
    for (var i = 2; i < items.length; i++) {
        while (isLeftTurn(items[hp - 1], items[hp], items[i]) <= 0) {
            if (hp > 1) {
                hp -= 1
            } else if (i == items.length - 1) {
                break
            } else {
                i += 1
            }
        }
        hp += 1
        var tmp = items[hp];
        items[hp] = items[i];
        items[i] = tmp;
    }

    return items.splice(0, hp-1);
}

/**
 * Calculates position of c based on ab vector. Returns positive if c is to
 * the left, 0 if they are aligned, negative for the right.
 * @author Nikolai Ershov http://habrahabr.ru/post/144571/
 *
 * @param [x, y] a
 * @param [x, y] b
 * @param [x, y] c
 * @returns {number}
 */
module.exports.isLeftTurn = isLeftTurn = function (a,b,c) {
    return (b[0]- a[0])*(c[1]-b[1])-(b[1]-a[1])*(c[0]-b[0])
}

function unique (value, index, self) {
    return self.indexOf(value) === index;
}
