/**
 * @desc Helper functions for parsing user input
 *
 * @author Arthem Nazarenko
 * @version 1.0.0
 */

/**
 * Parser string into array of Points [ { 2, 7 } ]
 *
 * @param string str
 * @returns {Array|Error}
 */
module.exports.parsePoints = function (str) {
    if (typeof str == 'string') {
        return str.split('\n').filter(notEmpty).map(toFloat);
    }
    throw new Error("String expected in parsePoints");
}

function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}

/**
 * Filters out empty elements from array
 *
 * @param string n
 * @returns {boolean}
 */
function notEmpty(n){
    return n !== '';
}

/**
 * Converts string line with points ['x','y'] to float [x,y]
 *
 * @param string n
 * @returns {[]|Error}
 */
function toFloat(n)  {
    var b = n.split(' ');
    var x = parseFloat(b[0]);
    var y = parseFloat(b[1]);
    if (isNaN(x) || isNaN(y)) {
        throw new Error("Invalid input data: x: " + x + "y: " + y)
    }
    return [x, y];
}