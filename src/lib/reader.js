/**
 * @desc Class for reading user input.
 *
 * @author Arthem Nazarenko
 * @version 1.0.0
 */

/**
 * Reads input file, if exists, and executes a callback function with file
 * content.
 *
 * @param string filename
 * @param function(error, content) callback
 * @returns {Error|void}
 */
module.exports.readFile = function(filename, callback) {
    var fs = require('fs');
    if (!(filename && fs.existsSync(filename))) {
        return new Error("Invalid filename. Usage: node src/index.js <path to file>");
    }
    if (typeof callback !== 'function') {
        return new Error("Callback is not a function");
    }
    fs.readFile(filename, callback);
}